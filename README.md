# Wireshark Windows Support Library Packaging Scripts

Wireshark depends on a number of
[third-party libraries](https://gitlab.com/wireshark/wireshark/-/wikis/Development/Support_library_version_tracking#windows), including GLib, Qt, Libgcrypt, c-ares, and others.
They come pre-packaged on Linux, but we have to build them ourselves on Windows.

These scripts are used to build some of the packages at
https://dev-libs.wireshark.org/windows/packages/
which are required to build Wireshark.
They in turn are downloaded by
[win-setup.ps1](https://gitlab.com/wireshark/wireshark/-/blob/master/tools/win-setup.ps1)
as part of the build process on Windows.

# Adding A New Package

To add a new package, do the following:

- Create a new directory for the package.
```
mkdir libfoo
```

- Copy an existing GitLab CI file to the new directory and add it to the repository and update it as needed.
  See "Updating A Package" below for variable definitions.
```
cp c-ares/gitlab-ci.yml libfoo
git add libfoo/gitlab-ci.yml
```

- Include the new gitlab-ci.yml in the top-level .gitlab-ci.yml.

# Updating A Package

- Update your package's version and git commit in its gitlab-ci.yml file.

__PORT_NAME__: The name of the port, e.g. c-ares or libpcap.
Usually not needed.

__PORT_VCPKG_COMMIT__: Vcpkg [doesn't let us specify a version number](https://github.com/microsoft/vcpkg/issues/1681), so we need to specify a commit that a) includes our desired package version and b) builds successfully.

__PKG_VERSION__: A version number for the generated .zip files. You should use the library's version number followed by a package revision, e.g. if the library's version number is "1.9.1" use "1.9.1-1".

- Comment and uncomment the includes in the top-level .gitlab-ci.yml as needed.

# Building A Package

To build a package, go to the [pipelines page](https://gitlab.com/wireshark/wireshark-vcpkg-scripts/-/pipelines) and build the desired job.
Test your package, and if it works add it to the [development libraries](https://gitlab.com/wireshark/wireshark-development-libraries) repository.
